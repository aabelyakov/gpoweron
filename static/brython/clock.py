#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 14.03.2013
# Purpose: Аналоговые часы
# Работает только рядом с brython.js, так как импортирует модули из 
# библиотеки brython/libs
#############################################################################
# gPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Губанова Ю.Д.
#############################################################################
__revision__ = "$Id: clock1.py 870 2013-03-14 11:34:14Z aabelyakov $"
#############################################################################
import time
import math
import datetime
#############################################################################
# Сокращения
sin, cos = math.sin, math.cos
#----------------------------------------------------------------------------
# Размеры холста
width, height = 250, 250 
#----------------------------------------------------------------------------
ray = 100 # clock ray
#============================================================================
def needle(angle,r1,r2,color="#000000"):
    # Нарисовать секундную стрелку для заданного угла angle заданным цветом
    # color r1 и r2 - проценты от часового луча 
    x1 = width/2-ray*cos(angle)*r1
    y1 = height/2-ray*sin(angle)*r1
    x2 = width/2+ray*cos(angle)*r2
    y2 = height/2+ray*sin(angle)*r2
    ctx.beginPath()
    ctx.strokeStyle = color
    ctx.moveTo(x1,y1)
    ctx.lineTo(x2,y2)
    ctx.stroke()
#enddef
#============================================================================
def set_clock():
    # Очистка круга для циферблата часов
    ctx.beginPath()
    ctx.fillStyle = "#FFF"
    ctx.arc(width/2,height/2,ray*0.89,0,2*math.pi)
    ctx.fill()
    #------------------------------------------------------------------------
    # Перерисовка часов
    show_hours()
    #------------------------------------------------------------------------
    # Вывод дня
    now = datetime.datetime.now()
    day = now.day
    ctx.font = "bold 14px Arial"
    ctx.textAlign = "center"
    ctx.textBaseline = "middle"
    ctx.fillStyle="#FFF"
    ctx.fillText(day,width*0.7,height*0.5)
    #------------------------------------------------------------------------
    ctx.lineWidth = 3
    hour = now.hour%12 + now.minute/60
    angle = hour*2*math.pi/12 - math.pi/2
    needle(angle,0.05,0.5)
    minute = now.minute
    angle = minute*2*math.pi/60 - math.pi/2
    needle(angle,0.05,0.85)
    ctx.lineWidth = 1
    second = now.second+now.microsecond/1000000
    angle = second*2*math.pi/60 - math.pi/2
    needle(angle,0.05,0.85,"#FF0000") # in red
#enddef
#============================================================================
time.set_interval(set_clock,100)
#----------------------------------------------------------------------------
canvas = doc["clock"]
#----------------------------------------------------------------------------
# Создание контекста холста
ctx = canvas.getContext("2d")
# Рисование окружности циферблата часов
ctx.beginPath()
ctx.lineWidth = 3
ctx.arc(width/2,height/2,ray,0,2*math.pi)
ctx.stroke()
#============================================================================
for i in range(60):
    # Создание "ресничек" для часов и минут 
    #------------------------------------------------------------------------
    ctx.lineWidth = 1
    #------------------------------------------------------------------------
    if i % 5 == 0:
        ctx.lineWidth = 3
    #endif
    #------------------------------------------------------------------------
    angle = i*2*math.pi/60 - math.pi/3
    x1 = width/2+ray*cos(angle)
    y1 = height/2+ray*sin(angle)
    x2 = width/2+ray*cos(angle)*0.9
    y2 = height/2+ray*sin(angle)*0.9
    
    ctx.beginPath()
    ctx.moveTo(x1,y1)
    ctx.lineTo(x2,y2)
    ctx.stroke()
#endfor
#============================================================================
def show_hours():
    """
    Показ часов
    """
    ctx.beginPath()
    ctx.arc(width/2,height/2,ray*0.05,0,2*math.pi)
    ctx.fillStyle = "#000"
    ctx.fill()
    #------------------------------------------------------------------------
    for i in range(1,13):
        angle = i * math.pi/6 - math.pi/2
        x3 = width/2 + ray*cos(angle) * 0.75
        y3 = height/2 + ray*sin(angle) * 0.75
        
        ctx.font = "20px Arial"
        ctx.textAlign = "center"
        ctx.textBaseline = "middle"
        ctx.fillText(i,x3,y3)
    #endfor
    #------------------------------------------------------------------------
    # Прямоугольник для показа дня
    ctx.fillStyle = "#000"
    ctx.fillRect(width*0.65, height*0.47, width*0.1, height*0.06)
#enddef
#############################################################################
show_hours()
#############################################################################
