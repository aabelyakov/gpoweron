#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 13.04.2013
#############################################################################
from sqlalchemy import MetaData
from sqlalchemy_schemadisplay import create_schema_graph
#############################################################################
name = "gpoweron"
#----------------------------------------------------------------------------
graph = create_schema_graph(
    metadata = MetaData('sqlite:///%s.sqlite' % name),
    show_datatypes = False, 
    show_indexes = False, 
    rankdir = 'LR', 
    concentrate = False 
)
#----------------------------------------------------------------------------
graph.write_png('%s.png' % name)
#############################################################################