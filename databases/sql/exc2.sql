    SELECT
        sht_id,
        enr_id,
        dt_beg,
        dt_end,
        mon,
        tarif,
        ed_izm,
        enr_a,
        enr_r,
        pwr_a,
        pwr_r,
        zapr
    FROM
        buf_enr
    WHERE
        dt_beg LIKE  '2012.10.02%'  AND
        sht_id = 2  AND
        id IN (
            SELECT 
                min(id)
            FROM 
                buf_enr
            GROUP BY 
                zapr
         )
        
    EXCEPT 
    
    SELECT
        sht_id,
        enr_id,
        dt_beg,
        dt_end,
        mon,
        tarif,
        ed_izm,
        enr_a,
        enr_r,
        pwr_a,
        pwr_r,
        zapr
    FROM
        par_enr
    WHERE
        dt_beg LIKE '2012.10.02%' AND 
	sht_id = 2
    ORDER BY
        dt_beg;