SELECT
  *
FROM
  buf_enr
WHERE
  dt_beg LIKE '2012.10.02%' AND 
  sht_id = 2 AND 
  id IN (
    SELECT min(id)
    FROM buf_enr
    GROUP BY zapr
  )

EXCEPT 

SELECT
  *
FROM
  par_enr
WHERE
  dt_beg like '2012.10.02%' AND sht_id = 2
ORDER BY
  dt_beg;