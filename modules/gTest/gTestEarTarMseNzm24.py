# !/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 25.01.2013
# Purpose: Тестирование функции gLib.EarTarMseNzm24()
#############################################################################
# gPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Губанова Ю.Д.
#############################################################################
__revision__ = "$Id: gTestListSht.py 596 2012-12-09 08:25:08Z aabelyakov $"
#############################################################################
import os
import sys
import datetime
#----------------------------------------------------------------------------
# Переключение текущего рабочего каталога - на каталог web2py
os.chdir('/home/aab/MyPython/web2py')
#print(os.getcwd())
#/home/aabelyakov/MyPython/web2py
#----------------------------------------------------------------------------
# Добавление к списку путей поиска python-модулей пути к каталогу web2py
sys.path.append(os.getcwd())
#print(sys.path)
#----------------------------------------------------------------------------
from applications.gpoweron.modules.gLib import EarTarMseNzm24
#############################################################################
# Внимание! 
# При тестировании должны задаваться даты, входящие в разрешённый период, 
# так как такая проверка осуществляется на уровне формы.
# Момент начала разрешённого периода - позапрошлый год, месяц - следующий за 
# текущим, число - 01. 
# Момент окончания разрешённого периода - текущий год, текущий месяц, число
# - последнее число месяца (то есть, 1-е число следующего месяца минус 1 
# день).
# Чтобы введённая пользователем дата начала заданного периода вошла в
# разрешённый период, она должна быть больше или равна дате начала
# разрешённого периода. 
# Чтобы введённая пользователем дата окончания заданного периода вошла в
# разрешённый период, она должна быть меньше или равна дате окончания
# разрешённого периода. 
#----------------------------------------------------------------------------
lRqs = EarTarMseNzm24("2B", "2012.4.3", "2013.3.31")
print "Количество запросов:", len(lRqs)
print lRqs
#----------------------------------------------------------------------------
lRqs = EarTarMseNzm24("2C", "2012.4.3", "2013.1.22")
print "Количество запросов:", len(lRqs)
print lRqs
#----------------------------------------------------------------------------
lRqs = EarTarMseNzm24("2D","2012.1.3", "2013.12.22")
print "Количество запросов:", len(lRqs)
print lRqs
#############################################################################
