#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 20.12.2013
# Purpose: Парсинг переменной окружения "VCAP_SERVICES" c хостинга AppFog
# Внимание!!!
# Переменные окружения хранятся в словаре os.environ
# И ключи, и значения из этого словаря должны иметь тип "string"
#############################################################################
__revision__ = "$Id$"
#############################################################################
import os
import sys
#----------------------------------------------------------------------------
# Переключение текущего рабочего каталога - на каталог web2py
os.chdir('/home/aab/MyPython/web2py')
#print(os.getcwd())
#/home/aabelyakov/MyPython/web2py
#----------------------------------------------------------------------------
# Добавление к списку путей поиска python-модулей пути к каталогу web2py
sys.path.append(os.getcwd())
#print(sys.path)
#----------------------------------------------------------------------------
from applications.gpoweron.modules import gLib as l 
#############################################################################
# Создание переменной окружения "VCAP_SERVICES" 
os.environ["VCAP_SERVICES"] = """{
    "postgresql-9.1": [{
        "name": "pgs",
        "label": "postgresql-9.1",
        "plan": "free",
        "tags": [
            "postgresql","postgresql-9.1","relational",
            "postgresql-9.1", "postgresql"
            ],
        "credentials": {
            "name": "d72c90fe871d94920a058595cb0c250a9",
            "host": "10.0.29.248",
            "hostname": "10.0.29.248",
            "port": 5432,
            "user": "u802e225430bc40bf9bf6af5b8a63073a",
            "username": "u802e225430bc40bf9bf6af5b8a63073a",
            "password": "pc19964763cd04433a9ba08c1e3ac9b3b"
        }
        }],

    "redis-2.2": [{
        "name": "red",
        "label": "redis-2.2",
        "plan": "free",
        "tags": ["redis","redis-2.2","key-value","nosql","redis-2.2","redis"],
        "credentials": {
            "hostname": "10.0.31.86",
            "host": "10.0.31.86",
            "port": 5176,
            "password": "d5d9c412-02c9-4d39-8130-0504dfc81770",
            "name": "e82cda3e-7610-482e-afd0-d2300171bc03"
        }
    }]
}"""
#----------------------------------------------------------------------------
print os.getenv("VCAP_SERVICES")
#############################################################################
if __name__ == '__main__':
    print l.Cred("postgresql-9.1")
    print l.Cred("redis-2.2")
#endif
#############################################################################