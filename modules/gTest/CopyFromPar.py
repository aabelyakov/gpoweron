# !/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 14.08.2012
# Purpose: Копирование данных из денормализованной таблицы par в таблицы 
# rqs и prm
#############################################################################
# gPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Губанова Ю.Д.
#############################################################################
__revision__ = "$Id$" 
#############################################################################
import os
import sys
#----------------------------------------------------------------------------
# Переключение текущего рабочего каталога - на каталог web2py
os.chdir('../../../..')
#print(os.getcwd())
#/home/aabelyakov/MyPython/web2py
#----------------------------------------------------------------------------
# Добавление к списку путей поиска python-модулей пути к текущему рабочему 
# каталогу
sys.path.append(os.getcwd())
#print(sys.path)
#----------------------------------------------------------------------------
# Импорт всех имён засоряет пространство имён, повышая вероятность 
# перекрытия глобальных переменных локальными
#from applications.gpoweron.modules.gLib import *
#----------------------------------------------------------------------------
from applications.gpoweron.modules import gLib as l
from applications.gpoweron.modules.gLib import db
#############################################################################
'''
Структура таблицы rqs
~~~~~~~~~~~~~~~~~~~~~
rqs.id, rqs.prf, rqs.zapr, rqs.op, rqs.znaim

Структура таблицы prm
~~~~~~~~~~~~~~~~~~~~~
prm.id, prm.rqs_id, prm.zapr, prm.naim, prm.yb, prm.ye, prm.typ, prm.col

Структура таблицы par
~~~~~~~~~~~~~~~~~~~~~
par.zapr, par.op, par.znaim, par.naim, par.yb, par.ye, par.typ, par.col
'''
#----------------------------------------------------------------------------
# Чтение всех неповторящихся запросов из таблицы par
oRows = db().select(
    db.par.zapr, 
    db.par.op,
    db.par.znaim,   
    orderby=db.par.zapr,
    groupby=db.par.zapr
)
#----------------------------------------------------------------------------
for oRow in oRows:
    #print "%2s" % oRow.zapr, oRow.znaim
    #------------------------------------------------------------------------
    # Вставка новых записей в таблицу rqs
    rqs_id = db.rqs.insert(zapr=oRow.zapr, op=oRow.op, znaim=oRow.znaim)
    #------------------------------------------------------------------------
    # Выборка из таблицы par всех записей, у которых код запроса 
    # совпадает с текущим кодом oRow.zapr
    rows = db(db.par.zapr==oRow.zapr).select(db.par.ALL)
    #------------------------------------------------------------------------
    for r in rows:
        # Цикл по записям из выборки rows
        #--------------------------------------------------------------------
        # Вставка новой записи в таблицу prm
        db.prm.insert(
            rqs_id=rqs_id,
            naim=r.naim, 
            yb=r.yb,
            ye=r.ye,
            typ=r.typ,
            col=r.col,
            dostup=r.dostup,
        )
    #endfor
#endfor  
#----------------------------------------------------------------------------
# Сброс буфера записей на диск
db.commit()
#----------------------------------------------------------------------------
# Создание таблицы par1 - двойника таблицы par
print "Копирование данных из денормализованной таблицы par в таблицы \
rqs и prm завершено"
#############################################################################
    