# !/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 24.07.2012
# Purpose: Проверка работы БД из приложения web2py
# Внимание! Запускать ссылку Z_TestDbGpoweron.py из каталога web2py       
#############################################################################
# gPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Губанова Ю.Д.
#                         (С) А.А.Беляков 2010
#############################################################################
__revision__ = "$Id$"
#############################################################################
import os
import sys
#----------------------------------------------------------------------------
# Переключение текущего рабочего каталога - на каталог web2py
os.chdir('/home/aab/MyPython/web2py')
#print(os.getcwd())
#/home/aabelyakov/MyPython/web2py
#----------------------------------------------------------------------------
# Добавление к списку путей поиска python-модулей пути к текущему рабочему 
# каталогу
sys.path.append(os.getcwd())
#print(sys.path)
#----------------------------------------------------------------------------
# Импортируем библиотеку gLib, используя её алиас - l
from applications.gpoweron.modules import gLib as l
from applications.gpoweron.models.g1_db import db
from gluon import *
#############################################################################
# 1-й вариант
# Используем алиас библиотеки
#print l.db.tables
'''
['obj', 'bus', 'krp', 'pdr', 'lgh', 'sht', 'vis', 'jrn', 'trf', 'enr',
'par', 'rqs', 'prm', 'pas', 'par_sht', 'par_jrn', 'par_enr', 'buf_enr',
'par_pwr', 'sel_adr', 'lst']
'''
#----------------------------------------------------------------------------
# 3-й вариант
print db.tables
'''
['obj', 'bus', 'krp', 'pdr', 'lgh', 'sht', 'vis', 'jrn', 'trf', 'enr',
'par', 'rqs', 'prm', 'pas', 'par_sht', 'par_jrn', 'par_enr', 'buf_enr',
'par_pwr', 'sel_adr', 'lst']
'''
#----------------------------------------------------------------------------
"""
print db
# <DAL uri="('sqlite://gpoweron.sqlite',)">
rows = db(db.sht.obj_id==2)(db.sht.ready==True).select(db.sht.id)
print rows
'''
sht.id
2
3
4
5
6
7
8
10
11
12
13
14
15
16
'''
#############################################################################
sDt = "2013.09.01"
iObjId = 2
query = (
    (db.par_enr.dt_beg.startswith(sDt)) &\
    (db.par_enr.obj_id==iObjId)
)
oRows = db(query).select()
print oRows
#############################################################################
db.define_table(
  'rrr',
  Field("obj_id", db.obj),
  Field("snaim", "string", length=30, default="", notnull=True),
  Field("naim", "string", length=256, default="", notnull=True),
  #migrate = "rrr.table"
  #migrate = False
)
print db.rrr.fields
#print db.rrr.insert(naim="Alex")
#print db(db.rrr.id >0).select()
db.rrr.truncate('RESTART IDENTITY CASCADE')
#print db(db.rrr).select()
#----------------------------------------------------------------------------
query = (
    (db.par_enr.dt_beg.like("2013.08.01" + "%")) &\
    (db.par_enr.sht_id==4) & \
    (db.par_enr.zapr.like("?" + "%"))
)
print db(query).count()
rows = db(db.par_pwr).select(db.par_pwr.ALL)
for row in rows:    
    print row.pwr_a
#endfor    

db.par_pwr.insert(pwr_a = 11.11)
db.commit()
sCodZap = l.RqsCode("C")
oRows = db((db.rqs.id==db.prm.rqs_id)&(db.rqs.zapr==sCodZap)).select(
    db.prm.yb,
    db.prm.ye,
    db.prm.typ,
    db.prm.col,
    db.prm.rqs_id,
    orderby = db.prm.yb,
)
print oRows
iBusId=8
#----------------------------------------------------------------------------
row1 = db(db.bus.id==iBusId).select().first()
#------------------------------------------------------------------------
print row1
dPort = {}
if row1:
    # Запись существует
    #--------------------------------------------------------------------
    # Присвоение значения глобальной переменной - Тайм-аут ожидания
    # ответа
    dPort['timeout'] = row1.timeout
    print dPort
#----------------------------------------------------------------------------
# Показать все имена из библиотеки gLib.py, подлежащие вызову
lName = dir(l)
for n in lName:
    # Цикл по всем именам из списка
    #------------------------------------------------------------------------
    # Функция eval() вычисляет свой аргумент в контексте globals и locals
    if callable(eval("l.%s" % n)):
        print n
    #endif
#endfor    
'''
AddRS23
AdrXreq
AllAddrs
ClosePort
ConvertParam
CrcException
Critical
Date
Debug
Dispatcher
EaTarMseNzm12
EaTarMseNzm24
EamNtpNow
EarFazTarNprMseNow
EarTarMseNzm24
EnrMseNow
EnrMseNtsNps
EnrMseNzm
EnrNzpNsp
Error
File2list
FiuInst
FmtEnr
FmtPwr
FrunbusException
G1
G2
G3
Info
InsertParams
IsValidRqs
Jrn
Ks
ListDat
ListHlf
ListProp
Master
Mess
MultStr2list
Naim
NoReplyException
NoRes
Num2st
NumPt
NumTp
PwrInst
PwrInstFaz
ReadCfgFile
RqsCode
St2num
Subst
TCP
Time
TimeCar
TimeoutException
Tm2Date
ToDate
ToHex
ToTime
Usage
ValidDate
VerVis
Warn
WriteCfgFile
XXX
Ymd2dmY
Ymd2dmy
'''
"""