#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 15.02.10
# Purpose: Модель - Соединение с БД и определения таблиц БД
#############################################################################
# gPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Губанова Ю.Д.
#                         (С) А.А.Беляков 2010
#############################################################################
__revision__ = "$Id$"
#############################################################################
import os
from gluon import *
from applications.gpoweron.modules import gLib as l
#############################################################################
# Имя СУБД
SUBD  = "sqlite"
#SUBD = "postgres"
#SUBD = "mysql"
#----------------------------------------------------------------------------
'''
import socket
hostname = socket.gethostname()
print hostname
'''
#----------------------------------------------------------------------------
HOST = "localhost"
#HOST = "cloud"
#----------------------------------------------------------------------------
if SUBD == "sqlite":
    # Создание словаря с параметрами доступа к серверу SQLite на localhost
    dCred = dict(
        subd = SUBD,
        user = "",
        password = "",
        host = "",
        name = "gpoweron.sqlite"
    )  
    #------------------------------------------------------------------------
    # Универсальный идентификатор ресурса
    URI = "%(subd)s://%(name)s" % dCred
    #------------------------------------------------------------------------
    # Каталог для файлов (*.table) с метаданными
    PATH = "applications/gpoweron/databases/SQLite"
    #------------------------------------------------------------------------
elif SUBD == "postgres":
    if HOST == "localhost":
        # Создание словаря с параметрами доступа к серверу PgSQL на localhost
        dCred = dict(
            subd = SUBD,
            user = "postgres",
            password = "cj.pybr",
            host = HOST,
            name = "gpoweron"
        )
        #--------------------------------------------------------------------
        # Универсальный идентификатор ресурса
        URI = "%(subd)s://%(user)s:%(password)s@%(host)s/%(name)s" % dCred
        #--------------------------------------------------------------------
        # Каталог для файлов (*.table) с метаданными
        PATH = "applications/gpoweron/databases/PgSQL"
        #--------------------------------------------------------------------
    elif HOST == "cloud":
        # Создание словаря с параметрами доступа к серверу PgSQL на AppFog
        dCred = l.Cred("postgresql-9.1")
        dCred["subd"] = SUBD
        #--------------------------------------------------------------------
        # Универсальный идентификатор ресурса
        URI = "%(subd)s://%(user)s:%(password)s@%(host)s/%(name)s" % dCred
        #--------------------------------------------------------------------
        # Каталог для файлов (*.table) с метаданными
        PATH = "applications/gpoweron/databases/PgSQL"
    #endif    
    #------------------------------------------------------------------------
elif SUBD == "mysql":
    # Создание словаря с параметрами доступа к серверу MySQL на localhost
    dCred = dict(
        subd = SUBD,
        user = "root",
        password = "dba",
        host = "localhost",
        name = "gpoweron"
    )
    #------------------------------------------------------------------------
    # Универсальный идентификатор ресурса
    URI = "%(subd)s://%(user)s:%(password)s@%(host)s/%(name)s" % dCred
    #------------------------------------------------------------------------
    # Каталог для файлов (*.table) с метаданными
    PATH = "applications/gpoweron/databases/MySQL"
#endif
#----------------------------------------------------------------------------
#print "URI:", URI
#----------------------------------------------------------------------------
# Объект соединения с основной БД
db = DAL(
    # Строка соединения с основной БД
    URI,
    #------------------------------------------------------------------------
    # Этот каталог должен существовать до момента создания БД
    folder = PATH
)
#############################################################################
#
#               С П Р А В О Ч Н Ы Е  Т А Б Л И Ц Ы
#
#############################################################################
# Cправочная таблица - ОБЪЕКТЫ
db.define_table(
    "obj",
    Field("snaim", "string", length=15, default="", unique=True),
    Field("naim", "string", length=512, default="", notnull=True),
    Field("address", "string", length=200, default="", notnull=True),
    Field("ready", "boolean", default=False, notnull=True),
    migrate='obj.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.obj.snaim.label = "СокрНаимОб"
db.obj.naim.label = "НаимОбъекта"
db.obj.address.label = "АдрОбъекта"
db.obj.ready.label = "Опросить"
#============================================================================
# Cправочная таблица - ШИНЫ
db.define_table(
    "bus",
    Field("obj_id", db.obj),
    Field("snaim", "string", length=30, default="", unique=True),
    Field("naim", "string", length=256, default="", notnull=True),
    Field("host", "string", length=15, default="", notnull=True),
    Field("port", "string", length=7, default="", notnull=True),
    Field("timeout", "integer", notnull=True),
    Field("grp_psw", "string", length=5,  default="", notnull=True),
    Field("ready", "boolean", default=False, notnull=True),
    migrate='bus.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.bus.obj_id.label = "СокрНаимОб"
db.bus.snaim.label =  "СокрНаимШины"
db.bus.naim.label = "НаимШины"
db.bus.host.label = "IpАдрTCPсерв"
db.bus.port.label = "ПортTCPсерв"
db.bus.timeout.label = "Тайм-аут"
db.bus.grp_psw.label = "ГрупПароль"
db.bus.ready.label = "Опросить"
#----------------------------------------------------------------------------
# Замена в выпадающем меню значения идентификтора записи из связанной 
# таблицы значением заданного поля этой таблицы
db.bus.obj_id.requires=IS_IN_DB(db,'obj.id','%(snaim)s')
#----------------------------------------------------------------------------
# Замена в таблице bus значения идентификтора записи из связанной таблицы
# значением заданного поля связанной таблицы
db.bus.obj_id.represent = lambda id, row: db.obj(id).snaim \
    if db.obj(id) else None
#============================================================================
# Cправочная таблица - КОРПУСА
db.define_table(
    "krp",
    Field("obj_id", db.obj),
    Field("snaim", "string", length=10, default="", unique=True),
    Field("naim", "string", length=256, default="", notnull=True),
    Field("lim", "decimal(10,2)", notnull=True),
    migrate='krp.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.krp.obj_id.label = "СокрНаимОб"
db.krp.snaim.label = "СокрНаимКорп"
db.krp.naim.label = "НаимКорпуса"
db.krp.lim.label = "Лимит"
#----------------------------------------------------------------------------
# Замена в выпадающем меню значения идентификтора записи из связанной 
# таблицы значением заданного поля этой таблицы
db.krp.obj_id.requires=IS_IN_DB(db,'obj.id','%(snaim)s')
#----------------------------------------------------------------------------
# Замена в таблице krp значения идентификтора записи из связанной таблицы
# значением заданного поля связанной таблицы
db.krp.obj_id.represent = lambda id, row: db.obj(id).snaim \
    if db.obj(id) else None
#============================================================================
# Cправочная таблица - ПОДРАЗДЕЛЕНИЯ
db.define_table(
    "pdr",
    Field("obj_id", db.obj),
    Field("snaim", "string", length=100, default="", unique=True),
    Field("naim", "string", length=256, default="", notnull=True),
    Field("lim", "decimal(10,2)", notnull=True),
    migrate='pdr.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.pdr.obj_id.label = "СокрНаимОб"
db.pdr.snaim.label = "СокрНаимПодр"
db.pdr.naim.label = "НаимПодр"
db.pdr.lim.label = "Лимит"
#----------------------------------------------------------------------------
# Замена в выпадающем меню значения идентификтора записи из связанной 
# таблицы значением заданного поля этой таблицы
db.pdr.obj_id.requires=IS_IN_DB(db,'obj.id','%(snaim)s')
#----------------------------------------------------------------------------
# Замена в таблице pdr значения идентификтора записи из связанной таблицы
# значением заданного поля связанной таблицы
db.pdr.obj_id.represent = lambda id, row: db.obj(id).snaim \
    if db.obj(id) else None
#============================================================================
# Cправочная таблица - НАЗНАЧЕНИЕ ЭНЕРГИИ
db.define_table(
    "lgh",
    Field("snaim", "string", length=30, default="", unique=True),
    Field("naim", "string", length=256, default="", notnull=True),
    migrate='lgh.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.lgh.snaim.label = "СокрНаим"
db.lgh.naim.label = "Наимен"
#============================================================================
# Cправочная таблица - СЧЁТЧИКИ
db.define_table(
    "sht",
    Field("obj_id", db.obj),
    Field("bus_id", db.bus),
    Field("krp_id", db.krp),
    Field("pdr_id", db.pdr),
    Field("lgh_id", db.lgh),
    Field("addr", "string", length=8, unique=True),
    Field("ready", "boolean", default=False, notnull=True),
    migrate='sht.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.sht.obj_id.label = "СокрНаимОб"
db.sht.bus_id.label = "СокрНаимШины"
db.sht.krp_id.label = "СокрНаимКорп"
db.sht.pdr_id.label = "СокрНаимПодр"
db.sht.lgh_id.label = "НазначЭнер"
db.sht.addr.label = "АдрПарольСч"
db.sht.ready.label = "Опросить"
#----------------------------------------------------------------------------
# Замена в выпадающем меню значения идентификтора записи из связанной 
# таблицы значением заданного поля этой таблицы
db.sht.obj_id.requires=IS_IN_DB(db, 'obj.id', '%(snaim)s')
db.sht.bus_id.requires=IS_IN_DB(db, 'bus.id', '%(snaim)s')
db.sht.krp_id.requires=IS_IN_DB(db, 'krp.id', '%(snaim)s')
db.sht.pdr_id.requires=IS_IN_DB(db, 'pdr.id', '%(snaim)s')
db.sht.lgh_id.requires=IS_IN_DB(db, 'lgh.id', '%(snaim)s')
db.sht.addr.requires = IS_LENGTH(
    8,  
    error_message='Должно быть число длиной 8-мь разрядов!'
)
#----------------------------------------------------------------------------
# Замена в таблице sht значения идентификтора записи из связанной таблицы
# значением заданного поля связанной таблицы
db.sht.obj_id.represent = lambda id, row: db.obj(id).snaim \
    if db.obj(id) else None
db.sht.bus_id.represent = lambda id, row: db.bus(id).snaim \
    if db.bus(id) else None
db.sht.krp_id.represent = lambda id, row: db.krp(id).snaim \
    if db.krp(id) else None
db.sht.pdr_id.represent = lambda id, row: db.pdr(id).snaim \
    if db.pdr(id) else None
db.sht.lgh_id.represent = lambda id, row: db.lgh(id).snaim \
    if db.lgh(id) else None
#============================================================================
# Cправочная таблица - ВАРИАНТЫ ИСПОЛНЕНИЯ СЧЁТЧИКОВ
db.define_table(
    "vis",
    Field("typ", "string", length=60, default="", notnull=True),
    Field("no_mpm", "string", length=5, default="", notnull=True),
    Field("wi_mpm", "string", length=20, default="", notnull=True),
    Field("tok", "string", length=6, default="", notnull=True),
    Field("fmt", "string", length=4, default="", notnull=True),
    Field("jki", "string", length=1, default="", notnull=True),
    migrate='vis.table'
)
#----------------------------------------------------------------------------
# Метки полей для отображения в формах
db.vis.typ.label = "ВарИспСч"
db.vis.no_mpm.label = "БезМПМ"
db.vis.wi_mpm.label = "С МПМ"
db.vis.tok.label = "Ток"
db.vis.fmt.label = "ФорматЖКИ"
db.vis.jki.label = "УстрОтобр"
#============================================================================
# Cправочная таблица - ЖУРНАЛЫ СОБЫТИЙ
db.define_table(
    "jrn",
    Field("njrn", "string", length=2, default="", unique=True),
    Field("snaim", "string", length=100, default="", notnull=True),
    Field("naim", "string", length=256, default="", notnull=True),
    migrate='jrn.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.jrn.njrn.label = "НомЖурнала"
db.jrn.snaim.label = "СокрНаимЖур"
db.jrn.naim.label = "ПолнНаимЖур"
#============================================================================
# Cправочная таблица - ТАРИФЫ
db.define_table(
    "trf",
    Field("ntrf", "string", length=1, default="", unique=True),
    Field("snaim", "string", length=30, default="", notnull=True),
    Field("time_beg", "string", length=8, default="", notnull=True),
    Field("time_end", "string", length=8, default="", notnull=True),
    migrate='trf.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.trf.ntrf.label = "НомТарифа"
db.trf.snaim.label = "СокрНаимТар"
db.trf.time_beg.label = "ВрНачТарифа"
db.trf.time_end.label = "ВрОкнТарифа"
#============================================================================
# Cправочная таблица - МАССИВЫ ЭНЕРГИИ
db.define_table(
    "enr",
    Field("nmas", "string", length=2, default="", unique=True),
    Field("period", "string", length=15, default="", notnull=True),
    Field("naim", "string", length=256, default="", notnull=True),
    migrate='enr.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.enr.nmas.label = "НомМассива"
db.enr.period.label = "ПериодНакоп"
db.enr.naim.label = "ПолнНаимМас"
#============================================================================
# Cправочная таблица - ЗАПРОСЫ К СЧЁТЧИКАМ
db.define_table(
    "rqs",
    Field("prf", "string", length=1, default="", notnull=True),
    Field("zapr", "string", length=2, default="", notnull=True),
    Field("op", "string", length=20, default="", notnull=True),
    Field("znaim", "string", length=250, default="", notnull=True),
    Field("depth", "integer", default=0, notnull=True),
    migrate='rqs.table',
)
#----------------------------------------------------------------------------
# Метки полей для отображения в формах
db.rqs.prf.label = "ПрефЗапроса"
db.rqs.zapr.label = "КодЗапроса"
db.rqs.op.label = "ОперЗапроса"
db.rqs.znaim.label = "НаимЗапроса"
db.rqs.depth.label = "ГлубХран"
#============================================================================
# Справочная таблица - ПАРАМЕТРЫ СЧЁТЧИКОВ В ОТВЕТАХ
db.define_table(
    "prm",
    Field("rqs_id", db.rqs),
    Field("zapr", "string", length=2, default="", notnull=True),
    Field("naim", "string", length=512, default="", notnull=True),
    Field("yb", "string", length=2, default="", notnull=True),
    Field("ye", "string", length=2, default="", notnull=True),
    Field("typ", "string", length=1, default="", notnull=True),
    Field("col", "string", length=30, default="", notnull=True),
    migrate="prm.table"
)
#----------------------------------------------------------------------------
# Метки полей для отображения в формах
db.prm.rqs_id.label = "СокрНаимЗапр"
db.prm.naim.label = "НаимПарСч"
db.prm.zapr.label = "КодЗапроса"
db.prm.yb.label = "НачБайтПар"
db.prm.ye.label = "КонБайтПар"
db.prm.typ.label = "ТипПарам"
db.prm.col.label = "НаимПоля"
#----------------------------------------------------------------------------
# Замена в выпадающем меню значения идентификтора записи из связанной 
# таблицы значением заданного поля этой таблицы
db.prm.rqs_id.requires=IS_IN_DB(db, 'rqs.id', '%(znaim)s')
#----------------------------------------------------------------------------
# Замена в таблице prm значения идентификтора записи из связанной таблицы
# значением заданного поля связанной таблицы
db.prm.rqs_id.represent = lambda id, row: db.rqs(id).znaim \
    if db.rqs(id) else None
#============================================================================
# Справочная таблица - ПРОПУСК ЗАПРОСА ДЛЯ ВЕРСИИ СЧЁТЧИКА
db.define_table(
    "pas",
    Field("rqs","string", length=6, default="", notnull=True),
    Field("ver", "string", length=2, default="", notnull=True),
    migrate="pas.table"
)
#----------------------------------------------------------------------------
# Метки полей для отображения в формах
db.pas.rqs.label = "Запрос"
db.pas.ver.label = "ВерсияСч"
#############################################################################
#
#                О П Е Р А Т И В Н Ы Е   Т А Б Л И Ц Ы
#
#############################################################################
# Оперативная таблица - ПАРАМЕТРЫ СЧЁТЧИКОВ
db.define_table(
    "par_sht",
    Field("obj_id", db.obj),
    Field("sht_id", db.sht, unique=True),
    Field("zav_nom", "string", length=8, default="", notnull=True),
    Field("naim", "string", length=30, default="", notnull=True),
    Field("ndn", "string", length=1, default="", notnull=True),
    Field("time", "string", length=8, default="", notnull=True),
    Field("date", "string", length=10, default="", notnull=True),
    Field("timet1", "string", length=8, default="", notnull=True),
    Field("timet2", "string", length=8, default="", notnull=True),
    Field("time_tz", "string", length=8, default="", notnull=True),
    Field("dli", "string", length=5, default="", notnull=True),
    Field("t", "string", length=1, default="", notnull=True),
    Field("cat", "string", length=2, default="", notnull=True),
    Field("lim_pwr", "string", length=10, default="", notnull=True),
    Field("lim_enr", "string", length=15, default="", notnull=True),
    Field("ver", "string", length=2, default="", notnull=True),
    Field("sezon", "string", length=10, default="", notnull=True),
    Field("vis", "string", length=4, default="", notnull=True),
    Field("n_kr", "string", length=1, default="", notnull=True),
    Field("t_kr", "string", length=8, default="", notnull=True),
    Field("d_kr", "string", length=10, default="", notnull=True),
    Field("n_on", "string", length=1, default="", notnull=True),
    Field("t_on", "string", length=8, default="", notnull=True),
    Field("d_on", "string", length=10, default="", notnull=True),
    Field("n_off", "string", length=1, default="", notnull=True),
    Field("t_off", "string", length=8, default="", notnull=True),
    Field("d_off", "string", length=10, default="", notnull=True),
    Field("out1", "string", length=10, default="", notnull=True),
    Field("przd", "string", length=10, default="", notnull=True),
    Field("n_ct", "string", length=1, default="", notnull=True),
    Field("t_ct", "string", length=8, default="", notnull=True),
    Field("d_ct", "string", length=10, default="", notnull=True),
    Field("tz", "string", length=6, default="", notnull=True),
    Field("nver", "string", length=8, default="", notnull=True),
    Field("frec", "string", length=4, default="", notnull=True),
    Field("ndn_v", "string", length=1, default="", notnull=True),
    Field("time_v", "string", length=8, default="", notnull=True),
    Field("date_v", "string", length=10, default="", notnull=True),
    Field("tochka", "string", length=30, default="", notnull=True),
    Field("date_isk", "string", length=10, default="", notnull=True),
    Field("ndn_isk", "string", length=1, default="", notnull=True),
    Field("stat1", "string", length=8, default="", notnull=True),
    Field("stat4", "string", length=8, default="", notnull=True),
    migrate='par_sht.table'
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.par_sht.obj_id.label = "СокрНаимОб"
db.par_sht.sht_id.label = "АдрПарольСч"
db.par_sht.zav_nom.label = "ЗавНомерСч"
db.par_sht.naim.label = "НаименСч"
db.par_sht.ndn.label = "ТекНомДняНед"
db.par_sht.time.label = "ТекВремя"
db.par_sht.date.label = "ТекДата"
db.par_sht.timet1.label = "ВрВклТ1"
db.par_sht.timet2.label = "ВрВклТ2"
db.par_sht.time_tz.label = "ВрВклТЗоны"
db.par_sht.dli.label = "Длит"
db.par_sht.t.label = "ТипТарифа"
db.par_sht.cat.label = "КатПотреб"
db.par_sht.lim_pwr.label = "ЛимитМощн"
db.par_sht.lim_enr.label = "ЛимитЭнер"
db.par_sht.ver.label = "ВерсияСч"
db.par_sht.sezon.label = "Сезон"
db.par_sht.vis.label = "ВарИспСч"
db.par_sht.n_kr.label = "КрНомДняНед"
db.par_sht.t_kr.label = "КрВремя"
db.par_sht.d_kr.label = "КрДата"
db.par_sht.n_on.label = "ВклНомДняНед"
db.par_sht.t_on.label = "ВклВремя"
db.par_sht.d_on.label = "ВклДата"
db.par_sht.n_off.label = "ВыклНомДняНед"
db.par_sht.t_off.label = "ВыклВремя"
db.par_sht.d_off.label = "ВыклДата"
db.par_sht.out1.label = "РежВых"
db.par_sht.przd.label = "ПрздДень"
db.par_sht.n_ct.label = "КорНомДняНед"
db.par_sht.t_ct.label = "КорВремя"
db.par_sht.d_ct.label = "КорДата"
db.par_sht.tz.label = "ВрЗадерж"
db.par_sht.nver.label = "ВерПО"
db.par_sht.frec.label = "ЧастСети"
db.par_sht.ndn_v.label = "НомДнНедИзг"
db.par_sht.time_v.label = "ВремяИзг"
db.par_sht.date_v.label = "ДатаИзг"
db.par_sht.tochka.label = "ТочкаУчёта"
db.par_sht.date_isk.label = "ДатаИсклДн"
db.par_sht.ndn_isk.label = "НомДнНедИсклДн"
db.par_sht.stat1.label = "1тар1тар"
db.par_sht.stat4.label = "1тар4тар"
#----------------------------------------------------------------------------
# Замена в выпадающем меню значения идентификтора записи из связанной 
# таблицы значением заданного поля этой таблицы
db.par_sht.obj_id.requires=IS_IN_DB(db,'obj.id','%(snaim)s')
db.par_sht.sht_id.requires=IS_IN_DB(db, 'sht.id', '%(addr)s')
#----------------------------------------------------------------------------
# Замена в таблице par_sht значения идентификтора записи из связанной 
# таблицы значением заданного поля связанной таблицы
db.par_sht.obj_id.represent = lambda id, row: db.obj(id).snaim \
    if db.obj(id) else None
db.par_sht.sht_id.represent = lambda id, row: db.sht(id).addr \
    if db.sht(id) else None
#============================================================================
# Оперативная таблица - ПАРАМЕТРЫ СОБЫТИЙ
db.define_table(
    "par_jrn",
    Field("obj_id", db.obj),
    Field("sht_id", db.sht),
    Field("jrn_id", db.jrn),
    Field("nz", "string", length=2, default=""),
    Field("dt1", "string", length=19, default="", notnull=True),
    Field("dt2", "string", length=19, default="", notnull=True),
    migrate='par_jrn.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.par_jrn.obj_id.label = "СокрНаимОб"
db.par_jrn.sht_id.label = "АдрПарольСч"
db.par_jrn.jrn_id.label = "СокрНаимЖур"
db.par_jrn.nz.label = "НомЗаписи"
db.par_jrn.dt1.label = "МомВыклВых"
db.par_jrn.dt2.label = "МомВклВозвр"
#----------------------------------------------------------------------------
# Замена в выпадающем меню значения идентификтора записи из связанной 
# таблицы значением заданного поля этой таблицы
db.par_jrn.obj_id.requires=IS_IN_DB(db,'obj.id','%(snaim)s')
db.par_jrn.sht_id.requires=IS_IN_DB(db, 'sht.id', '%(addr)s')
db.par_jrn.jrn_id.requires=IS_IN_DB(db, 'jrn.id', '%(snaim)s')
#----------------------------------------------------------------------------
# Замена в таблице par_jrn значения идентификтора записи из связанной 
# таблицы значением заданного поля связанной таблицы
db.par_jrn.obj_id.represent = lambda id, row: db.obj(id).snaim \
    if db.obj(id) else None
db.par_jrn.sht_id.represent = lambda id, row: db.sht(id).addr \
    if db.sht(id) else None
db.par_jrn.jrn_id.represent = lambda id, row: db.jrn(id).snaim \
    if db.jrn(id) else None
#============================================================================
# Оперативная таблица - ПАРАМЕТРЫ ЭНЕРГИИ
db.define_table(
    "par_enr",
    Field("obj_id", db.obj),
    Field("sht_id", db.sht),
    Field("enr_id", db.enr, default=0),
    Field("dt_beg", "string", length=19, default="", notnull=True),
    Field("dt_end", "string", length=19, default="", notnull=True),
    Field("mon", "string", length=2, default="", notnull=True),
    Field("faza", "string", length=1, default="", notnull=True),
    Field("tarif", "string", length=2, default="", notnull=True),
    Field("ed_izm", "string", length=5, default="", notnull=True),
    Field("enr_a", "double", default=0, notnull=True),
    Field("enr_r", "double", default=0, notnull=True),
    Field("pwr_a", "double", default=0, notnull=True),
    Field("pwr_r", "double", default=0, notnull=True),
    Field("zapr", "string", length=10, default="", notnull=True),
    migrate='par_enr.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.par_enr.obj_id.label = "СокрНаимОб"
db.par_enr.sht_id.label = "АдрПарольСч"
db.par_enr.enr_id.label = "ПериодНакЭн"
db.par_enr.dt_beg.label = "НачПериода"
db.par_enr.dt_end.label = "ОкнПериода"
db.par_enr.mon.label = "ЗадМесяц"
db.par_enr.faza.label = "Фаза"
db.par_enr.tarif.label = "Тариф"
db.par_enr.ed_izm.label = "ЕдИзм"
db.par_enr.enr_a.label = "АктЭнер"
db.par_enr.enr_r.label = "РктЭнер"
db.par_enr.pwr_a.label = "МаксАктМощн"
db.par_enr.pwr_r.label = "МаксРктМощн"
db.par_enr.zapr.label = "КодЗапроса"
#----------------------------------------------------------------------------
# Замена в выпадающем меню значения идентификтора записи из связанной 
# таблицы значением заданного поля этой таблицы
db.par_enr.obj_id.requires=IS_IN_DB(db,'obj.id','%(snaim)s')
db.par_enr.sht_id.requires=IS_IN_DB(db, 'sht.id', '%(addr)s')
db.par_enr.enr_id.requires=IS_IN_DB(db, 'enr.id', '%(period)s')
#----------------------------------------------------------------------------
# Замена в таблице par_enr значения идентификтора записи из связанной 
# таблицы значением заданного поля связанной таблицы
db.par_enr.obj_id.represent = lambda id, row: db.obj(id).snaim \
    if db.obj(id) else None
db.par_enr.sht_id.represent = lambda id, row: db.sht(id).addr \
    if db.sht(id) else None
db.par_enr.enr_id.represent = lambda id, row: db.enr(id).period \
    if db.enr(id) else None
#============================================================================
# Справочная таблица - ЗНАЧЕНИЯ ЭНЕРГИИ В МОМЕНТ ПУСКА АСТУЭ в эксплуатацию:
db.define_table(
  "ini",
  db.par_enr,
  migrate='ini.table'
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.ini.obj_id.label = "СокрНаимОб"
db.ini.sht_id.label = "АдрПарольСч"
db.ini.enr_id.label = "ПериодНакЭн"
db.ini.dt_beg.label = "НачПериода"
db.ini.dt_end.label = "ОкнПериода"
db.ini.mon.label = "ЗадМесяц"
db.ini.faza.label = "Фаза"
db.ini.tarif.label = "Тариф"
db.ini.ed_izm.label = "ЕдИзм"
db.ini.enr_a.label = "АктЭнер"
db.ini.enr_r.label = "РктЭнер"
db.ini.pwr_a.label = "МаксАктМощн"
db.ini.pwr_r.label = "МаксРктМощн"
db.ini.zapr.label = "КодЗапроса"
#----------------------------------------------------------------------------
# Замена в выпадающем меню значения идентификтора записи из связанной 
# таблицы значением заданного поля этой таблицы
db.ini.obj_id.requires=IS_IN_DB(db,'obj.id','%(snaim)s')
db.ini.sht_id.requires=IS_IN_DB(db, 'sht.id', '%(addr)s')
db.ini.enr_id.requires=IS_IN_DB(db, 'enr.id', '%(period)s')
#----------------------------------------------------------------------------
# Замена в таблице ini значения идентификтора записи из связанной 
# таблицы значением заданного поля связанной таблицы
db.ini.obj_id.represent = lambda id, row: db.obj(id).snaim \
    if db.obj(id) else None
db.ini.sht_id.represent = lambda id, row: db.sht(id).addr \
    if db.sht(id) else None
db.ini.enr_id.represent = lambda id, row: db.enr(id).period \
    if db.enr(id) else None
  
#============================================================================
# Оперативная таблица - ПАРАМЕТРЫ МОЩНОСТИ
db.define_table(
    "par_pwr",
    Field("obj_id", db.obj),
    Field("sht_id", db.sht),
    Field("dt_now", "string", length=19, default="", notnull=True),
    Field("ed_izm", "string", length=5, default="", notnull=True),
    Field("pwr_a", "double", default=0, notnull=True),
    Field("pwr_aa", "double", default=0, notnull=True),
    Field("pwr_ab", "double", default=0, notnull=True),
    Field("pwr_ac", "double", default=0, notnull=True),
    Field("pwr_r", "double", default=0, notnull=True),
    Field("pwr_ra", "double", default=0, notnull=True),
    Field("pwr_rb", "double", default=0, notnull=True),
    Field("pwr_rc", "double", default=0, notnull=True),
    Field("zapr", "string", length=10, default="", notnull=True),
    migrate='par_pwr.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.par_pwr.obj_id.label = "СокрНаимОб"
db.par_pwr.sht_id.label = "АдрПарольСч"
db.par_pwr.dt_now.label = "ВрОтпИзмер"
db.par_pwr.ed_izm.label = "ЕдИзм"
db.par_pwr.pwr_a.label  = "МощнАкт"
db.par_pwr.pwr_aa.label = "МощнАктФA"
db.par_pwr.pwr_ab.label = "МощнАктФB"
db.par_pwr.pwr_ac.label = "МощнАктФC"
db.par_pwr.pwr_r.label  = "МощнРеакт"
db.par_pwr.pwr_ra.label = "МощнРктФA"
db.par_pwr.pwr_rb.label = "МощнРктФB"
db.par_pwr.pwr_rc.label = "МощнРктФC"
db.par_pwr.zapr.label  = "КодЗапроса"
#----------------------------------------------------------------------------
# Замена в выпадающем меню значения идентификтора записи из связанной 
# таблицы значением заданного поля этой таблицы
db.par_pwr.obj_id.requires=IS_IN_DB(db,'obj.id','%(snaim)s')
db.par_pwr.sht_id.requires=IS_IN_DB(db, 'sht.id', '%(addr)s')
#----------------------------------------------------------------------------
# Замена в таблице par_pwr значения идентификтора записи из связанной 
# таблицы значением заданного поля связанной таблицы
db.par_pwr.obj_id.represent = lambda id, row: db.obj(id).snaim \
    if db.obj(id) else None
db.par_pwr.sht_id.represent = lambda id, row: db.sht(id).addr \
    if db.sht(id) else None
#============================================================================
# Оперативная таблица - ПАРАМЕТРЫ ФАЗНЫХ ТОКОВ И НАПРЯЖЕНИЙ
db.define_table(
    "par_fiu",
    Field("obj_id", db.obj),
    Field("sht_id", db.sht),
    Field("dt_now", "string", length=19, default="", notnull=True),
    Field("ed_izm", "string", length=5, default="", notnull=True),
    Field("i_fa", "double", default=0, notnull=True),
    Field("i_fb", "double", default=0, notnull=True),
    Field("i_fc", "double", default=0, notnull=True),
    Field("u_fa", "double", default=0, notnull=True),
    Field("u_fb", "double", default=0, notnull=True),
    Field("u_fc", "double", default=0, notnull=True),
    Field("zapr", "string", length=10, default="", notnull=True),
    migrate='par_fiu.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.par_fiu.obj_id.label = "СокрНаимОб"
db.par_fiu.sht_id.label = "АдрПарольСч"
db.par_fiu.dt_now.label = "ВрОтпИзмер"
db.par_fiu.ed_izm.label = "ЕдИзм"
db.par_fiu.i_fa.label  = "ТокФА"
db.par_fiu.i_fb.label = "ТокФВ"
db.par_fiu.i_fc.label = "ТокФC"
db.par_fiu.u_fa.label = "НапрФА"
db.par_fiu.u_fb.label = "НапрФB"
db.par_fiu.u_fc.label = "НапрФC"
db.par_fiu.zapr.label  = "КодЗапроса"
#----------------------------------------------------------------------------
# Замена в выпадающем меню значения идентификтора записи из связанной 
# таблицы значением заданного поля этой таблицы
db.par_fiu.obj_id.requires=IS_IN_DB(db,'obj.id','%(snaim)s')
db.par_fiu.sht_id.requires=IS_IN_DB(db, 'sht.id', '%(addr)s')
#----------------------------------------------------------------------------
# Замена в таблице par_pwr значения идентификтора записи из связанной 
# таблицы значением заданного поля связанной таблицы
db.par_fiu.obj_id.represent = lambda id, row: db.obj(id).snaim \
    if db.obj(id) else None
db.par_fiu.sht_id.represent = lambda id, row: db.sht(id).addr \
    if db.sht(id) else None
#============================================================================
# Оперативная таблица - СПИСКИ ЗАПРОСОВ
db.define_table(
    "lst",
    Field("im", "string", length=50, default="", unique=True),
    Field("naim", "string", length=1024, default="", notnull=True),
    Field("lst", "text"),
    Field("isx", "text"),
    Field('lst_up', 'upload', uploadfield='isx', notnull=True),
    migrate='lst.table',
)
#----------------------------------------------------------------------------
# Метки полей для отображения в формах
db.lst.im.label = "ИмяСписЗапросов"
db.lst.naim.label = "ОписСписЗапросов"
db.lst.lst.label = "ВыбранЗапросы"
db.lst.isx.label = "СодержЗагрФайла"
db.lst.lst_up.label = "ЗагрИзФайла"
#============================================================================
# Оперативная таблица - ОТЧЁТЫ
db.define_table(
  'rpt',
  Field("obj_id", db.obj),
  Field("snaim", "string", length=30, default="", notnull=True),
  Field("naim", "string", length=256, default="", notnull=True),
  Field("dt_beg", "string", length=19, default="", notnull=True),
  Field("dt_end", "string", length=19, default="", notnull=True),
  Field('image', 'blob'),
  migrate='rpt.table',
)
#----------------------------------------------------------------------------
# Метки полей таблицы для отображения в формах
db.rpt.obj_id.label = "СокрНаимОб"
db.rpt.snaim.label = "СокрНаимОтч"
db.rpt.naim.label = "НаимОтчёта"
db.rpt.dt_beg.label = "НачОтчПер"
db.rpt.dt_end.label = "ОкнОтчПер"
db.rpt.image.label = "Отчёт"
#----------------------------------------------------------------------------
# Замена в выпадающем меню значения идентификтора записи из связанной 
# таблицы значением заданного поля этой таблицы
db.rpt.obj_id.requires=IS_IN_DB(db,'obj.id','%(snaim)s')
#----------------------------------------------------------------------------
# Замена в таблице bus значения идентификтора записи из связанной таблицы
# значением заданного поля связанной таблицы
db.rpt.obj_id.represent = lambda id, row: db.obj(id).snaim \
    if db.obj(id) else None
#============================================================================