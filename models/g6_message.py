#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 06.12.2013
# Purpose: Модель для приёма и обработки сообщений
#############################################################################
# gPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Губанова Ю.Д.
#                         (С) А.А.Беляков 2010
#############################################################################
__revision__ = "$Id$"
#############################################################################
#import os
from hotqueue import HotQueue
from applications.gpoweron.modules import gLib as l
#############################################################################
# Создание словаря с параметрами доступа к серверу redis на AppFog
dCred = l.Cred("redis-2.2")
#----------------------------------------------------------------------------
# Соединение с очередью ошибок qerror на сервере redis
qerror = HotQueue(
    "qerror",
    host=dCred.get("host") or "localhost", 
    port=dCred.get("port") or 6379, 
    password=dCred.get("password"),
    db=0
)
#----------------------------------------------------------------------------
# Соединение с очередью событий qevent на сервере redis
qevent = HotQueue(
    "qevent",     
    host=dCred.get("host") or "localhost", 
    port=dCred.get("port") or 6379,  
    password=dCred.get("password"),
    db=0
)
#----------------------------------------------------------------------------
#print "qerror:", qerror.get()
#print "qevent:", qevent.get()
#############################################################################
