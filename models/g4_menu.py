#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 25.02.2010
# Purpose: Модель - Главное меню приложения gPowerOn
#============================================================================
# gPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Губанова Ю.Д.
#                         (С) А.А.Беляков 2010
#============================================================================
__revision__ = "$Id$"
#============================================================================
# Имя приложения
app = request.application
#----------------------------------------------------------------------------
# Имя контроллера
ctr = request.controller
#----------------------------------------------------------------------------
# Имя шаблона
view = response.view
#----------------------------------------------------------------------------
# Заголовок приложения для базового шаблона
response.title = "gPowerOn"
#----------------------------------------------------------------------------
# Подзаголовок приложения 
response.subtitle = 'Автоматизированная Система Технического Учёта \
Электроэнергии'
#----------------------------------------------------------------------------
# Автор приложения 
response.author = "Беляков Анатолий Алексеевич"
#----------------------------------------------------------------------------
# Узнай больше о мета-атрибутах на сайте:
# http://dev.w3.org/html5/markup/meta.name.html
response.meta.author = 'А.А.Беляков <aabelyakov@mail.ru>'
response.meta.description = 'a cool new app'
response.meta.keywords = 'web2py, python, framework'
response.meta.generator = 'Web2py Web Framework'
response.meta.copyright = 'Авторское право (c) А.А.Беляков 2010'
#============================================================================
ADMIN_MENU = False
#----------------------------------------------------------------------------
# Проверим, что последний вошедший пользователь является членом группы
# group_id = session.adm_id
ADMIN_MENU = auth.has_membership(session.adm_id)
#----------------------------------------------------------------------------
# Главное меню приложения для пользователей из группы usr
response.menu = [
    ['Начальная страница', False,  URL(
        app,
        "default",
        "index")],

    ['Выбрать объект', False,  URL(
        app,
        "select",
        "sel_obj")],
    
    ['Задать отчётный период', False,  URL(
        app,
        "reports",
        "dat_rpt")],
   
    ['Выгрузить отчёт', False, "", [
        ['Одиночные параметры', False, URL(
            app,
            'reports',
            'dwn_rpt_op')],
        
        ['ПЧ энергия по объекту', False, URL(
            app,
            'reports',
            'dwn_rpt_obj')],
        
        ['ПЧ энергия по корпусу', False, URL(
            app,
            'reports',
            'dwn_rpt_krp')],
        
        ['ПЧ энергия по подразделению', False, URL(
            app,
            'reports',
            'dwn_rpt_pdr')],
        
        ['ПЧ энергия по СИЛЕ и СВЕТУ', False, URL(
            app,
            'reports',
            'dwn_rpt_lgh')],
        ]],
        
    ['Смотреть диаграмму', False, "", [
        ['ПЧ энергия по объекту', False, URL(
            app,
            'charts',
            'view_chr_obj')],
        
        ['ПЧ энергия по корпусу', False, URL(
            app,
            'charts',
            'view_chr_krp')],
        
        ['ПЧ энергия по подразделению', False, URL(
            app,
            'charts',
            'view_chr_pdr')],
        
        ['ПЧ энергия по СИЛЕ и СВЕТУ', False, URL(
            app,
            'charts',
            'view_chr_lgh')],
        ]],
    
    ['Выгрузить диаграмму', False, "", [
        ['ПЧ энергия по объекту', False, URL(
            app,
            'charts',
            'dwn_chr_obj')],
            
        ['ПЧ энергия по корпусу', False, URL(
            app,
            'charts',
            'dwn_chr_krp')],
            
        ['ПЧ энергия по подразделению', False, URL(
            app,
            'charts',
            'dwn_chr_pdr')],
        
        ['ПЧ энергия по СИЛЕ и СВЕТУ', False, URL(
            app,
            'charts',
            'dwn_chr_lgh')],
        ]],
]
#============================================================================
def adm_menu():
    # Главное меню приложения для пользователей из группы adm
    #------------------------------------------------------------------------
    response.menu = [
        ['Начальная страница', False,  URL(
            app,
            "default",
            "index")],
    
        ['Выбрать объект', False,  URL(
            app,
            "select",
            "sel_obj")],
        
        ['Выбрать шины', False,  URL(
            app,
            "select",
            "sel_bus")],
    
        ["Выбрать счётчики", False, URL(
            app,
            'select',
            'sel_adr')],
    
        ['Работать с запросами', False, "", [
            ["Выбрать", False, URL(
                app,
                'select',
                'sel_rqs')],
    
            ["Править", False, URL(
                app,
                'select',
                'upd_atr_rqs')],
    
            ["Создать", False, URL(
                app,
                "select",
                "crt_rqs")],
    
            ["Загрузить", False, URL(
                app,
                'select',
                'upl_rqs')],
    
            ["Выгрузить", False, URL(
                app,
                "select",
                "dwn_rqs")],
            ]],
    
        ['Запустить опрос', False, URL(
            app,
            'opros',
            'opros')],
    
        ['Работать с записями', False, URL(
            app,
            'records',
            'opr_rec')],
    
        ['Задать отчётный период', False,  URL(
            app,
            "reports",
            "dat_rpt")],
    
        ['Создать отчёт', False, "", [
            ['Одиночные параметры', False, URL(
                app,
                'reports',
                'crt_rpt_op')],
            
            ['ПЧ энергия по объекту', False, URL(
                app,
                'reports',
                'crt_rpt_obj')],
    
            ['ПЧ энергия по корпусу', False, URL(
                app,
                'reports',
                'crt_rpt_krp')],
    
            ['ПЧ энергия по подразделению', False, URL(
                app,
                'reports',
                'crt_rpt_pdr')],
            
            ['ПЧ энергия по СИЛЕ и СВЕТУ', False, URL(
                app,
                'reports',
                'crt_rpt_lgh')],
            ]],
        
        ['Выгрузить отчёт', False, "", [
            ['Одиночные параметры', False, URL(
                app,
                'reports',
                'dwn_rpt_op')],
            
            ['ПЧ энергия по объекту', False, URL(
                app,
                'reports',
                'dwn_rpt_obj')],
            
            ['ПЧ энергия по корпусу', False, URL(
                app,
                'reports',
                'dwn_rpt_krp')],
            
            ['ПЧ энергия по подразделению', False, URL(
                app,
                'reports',
                'dwn_rpt_pdr')],
            
            ['ПЧ энергия по СИЛЕ и СВЕТУ', False, URL(
                app,
                'reports',
                'dwn_rpt_lgh')],
            ]],
            
        ['Смотреть диаграмму', False, "", [
            ['ПЧ энергия по объекту', False, URL(
                app,
                'charts',
                'view_chr_obj')],
            
            ['ПЧ энергия по корпусу', False, URL(
                app,
                'charts',
                'view_chr_krp')],
            
            ['ПЧ энергия по подразделению', False, URL(
                app,
                'charts',
                'view_chr_pdr')],
            
            ['ПЧ энергия по СИЛЕ и СВЕТУ', False, URL(
                app,
                'charts',
                'view_chr_lgh')],
            ]],
        
        ['Выгрузить диаграмму', False, "", [
            ['ПЧ энергия по объекту', False, URL(
                app,
                'charts',
                'dwn_chr_obj')],
                
            ['ПЧ энергия по корпусу', False, URL(
                app,
                'charts',
                'dwn_chr_krp')],
            
            ['ПЧ энергия по подразделению', False, URL(
                app,
                'reports',
                'dwn_chr_pdr')],
            
            ['ПЧ энергия по СИЛЕ и СВЕТУ', False, URL(
                app,
                'charts',
                'dwn_chr_lgh')],
            ]],
        
        ['Экспортир. данные', False, "", [
            ["Из одной таблицы", False, URL(
                app,
                'csvfile',
                'exp_csv_one')],
    
            ['Изо всех справ. таблиц', False, URL(
                app,
                'csvfile',
                'exp_csv_all')],
            ]],
    
        ['Импортир. данные', False, "", [
            ['В одну таблицу', False, URL(
                app,
                'csvfile',
                'imp_csv_one')],
    
            ['Во все справ. таблицы', False, URL(
            app,
            'csvfile',
            'imp_csv_all')],
        ]],
        
        ['Чистить таблицу', False,  URL(
            app,
            "edit",
            "trn_one")],
   
        ['Править таблицу', False,  URL(
            app,
            "edit",
            "sel_spr")],
    
        ['Смотреть таблицу', False,  URL(
            app,
            "edit",
            "sel_opr")],
        
        ['Отправить почту', False,  URL(
            app,
            "mail",
            "send_mail")],

        ['Смотреть журнал', False,  URL(
            app,
            "edit",
            "view_log")],
        
        ["Сайт", False, URL(
            "admin",
            "default",
            "site")],
            
        ['Тесты', False, "", [
            ['Clock2', False, URL(
                app,
                'tests',
                'test15')],
            
            ['Xlsx', False, URL(
                app,
                'tests',
                'test24')],
            
            ['Получас', False, URL(
                app,
                'tests',
                'test25')],

            ['Планировщик', False, URL(
                app,
                'tests',
                'test26')],
            
            ['HTML', False, URL(
                app,
                'tests',
                'test18')],
            
            ['CSV', False, URL(
                app,
                'tests',
                'test19')],
                        
            ['Sht', False, URL(
                app,
                'tests',
                'test28')],
            
            ['Log', False, URL(
                app,
                'tests',
                'test29')],
            
            ['XML', False, URL(
                app,
                'tests',
                'test30')],
            ]],
    ]
#enddef    
#----------------------------------------------------------------------------
if ADMIN_MENU:
    # Установлен флаг - Показать меню администратора
    #------------------------------------------------------------------------
    # Вызов функции показа главного меню приложения для группы adm
    adm_menu()
#endif
#============================================================================
'''
response.menu += [
    ['Тесты', False, "", [
        ['Просмотр rqs', False, URL(
            app,
            'tests',
            'test1')],
    
        ['Просмотр rqs=prm', False, URL(
            app,
            'tests',
            'test2')],

        ['SQLFORM', False, URL(
            app,
            'tests',
            'test3',
            args=["prm", "1"])],
    
        ['jQUERY', False, URL(
            app,
            'tests',
            'test4')],
    
        ['CRUD_tables', False, URL(
            app,
            'tests',
            'test5')],
    
        ['CRUD_read', False, URL(
            app,
            'tests',
            'test10')],
    
        ['CRUD_insert', False, URL(
            app,
            'tests',
            'test6',
            args=["prm"])],
    
        ['CRUD_update', False, URL(
            app,
            'tests',
            'test7',
            args=["par_sht", "2"])],
    
        ['GRID', False, URL(
            app,
            'tests',
            'test8')],
    
        ['SMART_GRID', False, URL(
            app,
            'tests',
            'test9')],

        ['SQLTABLE_par_sht', False, URL(
             app,
             'tests',
             'test11',
             args=["par_sht"])],            
    
        ['MultuSelect', False, URL(
            app,
            'tests',
            'test12')],
    
        ['Tables', False, URL(
            app,
            'tests',
            'test13')],

        ['Clock1', False, URL(
            app,
            'tests',
            'test14')],
    
        ['Clock2', False, URL(
            app,
            'tests',
            'test15')],
        
        ['Test16', False, URL(
            app,
            'tests',
            'test16')],
                    
        ['Multi-Forms', False, URL(
            app,
            'tests',
            'test17')],
        
        ['Test18', False, URL(
            app,
            'tests',
            'test18')],
        
        ['Test19', False, URL(
            app,
            'tests',
            'test19')],
        
        ['XLWT', False, URL(
            app,
            'tests',
            'test20')],
        
        ['XML', False, URL(
            app,
            'tests',
            'test21')],
        
        ['rows.xml()', False, URL(
            app,
            'tests',
            'test22')],
        
        ['pygal', False, URL(
            app,
            'tests',
            'test23')],
        ]],
    ]
    '''
#############################################################################

